import React from 'react';

const Triangle = () => {
    return (
        <svg version="1.2" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
            x="0px" y="0px">
            <polygon width="100%" height="100%" points="397,0 397,733 0,490.5 "/>
        </svg>
    )
}

export default Triangle;