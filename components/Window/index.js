import React from 'react';


const Window = ( props ) => {
    return (
        <article className='window'>
            <header className='window__title-bar'>
                <h2 className='window__title'>{ props.title }</h2>
            </header>
            <div className='window__content'>
                { props.children }
            </div>
            <footer className='window__footer'>
                
            </footer>
        </article>
    );
}

export default Window;
