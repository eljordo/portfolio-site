import React from 'react';
import css from './../styles/css.sass';
import Triangle from './../components/GeoElements/Triangle';
//import Projects from './../containers/Projects';


const Home  = ( props ) => {
    return (
        <main>
            <div className='grid'>
                <div className='grid__line'></div>
                <div className='grid__line'></div>
                <div className='grid__line'></div>
                <div className='grid__line'></div>
                <div className='grid__line'></div>
            </div>
            <div className='opening-hero'>
                <div className='opening-hero__intro'>
                    <h1>Jordan J Tidwell</h1>
                    <p className='opening-hero__lead'>Builder of things on the internet.</p>
                </div>
            </div>
        </main>
    );
}
//
export default Home;