import React, {Component} from 'react';
import Window from './../../components/Window';



const projects = [
    {
        title: 'Cool Project',
        body: 'Aenean at condimentum sapien. Suspendisse ac quam at mi laoreet feugiat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.'
    },
    {
        title: 'Another Project',
        body: 'Aenean at condimentum sapien. Suspendisse ac quam at mi laoreet feugiat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.'
    },
    {
        title: 'Another Project 2',
        body: 'Aenean at condimentum sapien. Suspendisse ac quam at mi laoreet feugiat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.'
    },
];

class ProjectWindow extends Component {

    ref = React.createRef();

    componentDidMount() {
        console.log( this.ref.current )
    }

    render() {
        return (
            <Window ref={ this.ref } title={ this.props.title }>
                <p>{ this.props.body }</p>
            </Window>
        );
    }
}

const Projects = ( props ) => {
    return (
        <div className='projects'>
            { projects.map(( project, index ) => <ProjectWindow key={ index } { ... project } />) }
        </div>
    );
}

export default Projects;